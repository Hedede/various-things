#include "MainWindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent)
{
	auto tab = new QTabWidget;
	tab->addTab(new QWidget, "Test");
	setCentralWidget(tab);

	qDebug() << tab->palette().brush(tab->backgroundRole()).color();
	qDebug() << tab->palette().color(tab->backgroundRole());
}

MainWindow::~MainWindow()
{
}
